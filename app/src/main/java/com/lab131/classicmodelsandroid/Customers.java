package com.lab131.classicmodelsandroid;

import com.lab131.classicmodelsandroid.domain.Customer;

import java.util.ArrayList;

/**
 * Created by chadj on 6/13/16.
 */
public class Customers {
	private ArrayList<Customer> customers;

	public ArrayList<Customer> getCustomers() {
		return customers;
	}

	public void setCustomers(ArrayList<Customer> customers) {
		this.customers = customers;
	}
}
