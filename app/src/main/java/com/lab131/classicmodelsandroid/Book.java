package com.lab131.classicmodelsandroid;

public class Book {
   private static final long serialVersionUID = 298443116673285961L;

   private String isbn;
   
   private String title;
   
   private int copiesAvailable;
   
   protected Book() { 
   }
   
   public Book(final String isbn, final String title, int copiesAvailable) {
      this.isbn = isbn;
      this.title = title;
      this.copiesAvailable = copiesAvailable;
   }

   public String getIsbn() {
      return isbn;
   }

   public String getTitle() {
      return title;
   }

   public int getCopiesAvailable() {
      return copiesAvailable;
   }

   public void setCopiesAvailable(int copiesAvailable) {
      this.copiesAvailable = copiesAvailable;
   }

 }
