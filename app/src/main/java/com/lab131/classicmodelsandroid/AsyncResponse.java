package com.lab131.classicmodelsandroid;

/**
 * Created by chadj on 5/6/16.
 */
public interface AsyncResponse {
	void processFinish(Object output);
}
